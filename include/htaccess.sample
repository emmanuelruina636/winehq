# default permissions
allow from all

# Set Our Default Directory Index
DirectoryIndex site.php index.html index.shtml index.cgi index.php

# Website Offline
#RewriteEngine on
#Rewritebase /
#RewriteRule \.(html|jpg|gif|png|css)$ - [L]
#RewriteRule ^(.*) /down.html [R=302,L]

# 404 not found document
ErrorDocument 404 http://www.winehq.org/404

# Tell PHP how we want to handle global vars
php_value register_globals 0

# We want to use ";" as variable separator (more XML compliant)
php_value arg_separator.output ";"
php_value arg_separator.input ";&"

# force the php to accept PATH_INFO
<Files ~ (site.php)$>
  AcceptPathInfo On
  ForceType application/x-httpd-php
</Files>

# redirect old winehq pages to new locations
RedirectPermanent /index.php                    https://www.winehq.org/
RedirectPermanent /about.shtml                  https://www.winehq.org/about
RedirectPermanent /about/index.php              https://www.winehq.org/about
RedirectPermanent /community.shtml              https://www.winehq.org/contributions
RedirectPermanent /devel                        https://www.winehq.org/getinvolved
RedirectPermanent /dev                          https://www.winehq.org/getinvolved
RedirectPermanent /dev.shtml                    https://www.winehq.org/getinvolved
RedirectPermanent /dev.html                     https://www.winehq.org/getinvolved
RedirectPermanent /devstart.shtml               https://www.winehq.org/getinvolved
RedirectPermanent /download                     https://wiki.winehq.org/Download
RedirectPermanent /download/index.php           https://wiki.winehq.org/Download
RedirectPermanent /download.shtml               https://wiki.winehq.org/Download
RedirectPermanent /download.html                https://wiki.winehq.org/Download
RedirectPermanent /download-mirrors.html        https://wiki.winehq.org/Download
RedirectPermanent /download-mirrors.shtml       https://wiki.winehq.org/Download
RedirectPermanent /site/download-deb            https://wiki.winehq.org/Debian
RedirectPermanent /download/debian              https://wiki.winehq.org/Debian
RedirectPermanent /download/ubuntu              https://wiki.winehq.org/Ubuntu
RedirectPermanent /site/getting_help            https://www.winehq.org/help
RedirectPermanent /support.shtml                https://www.winehq.org/help
RedirectPermanent /News/status.html             https://www.winehq.org/status
RedirectPermanent /wiki                         https://wiki.winehq.org/
RedirectPermanent /faq                          https://wiki.winehq.org/FAQ
RedirectPermanent /site/faq                     https://wiki.winehq.org/FAQ
RedirectPermanent /FAQ                          https://wiki.winehq.org/FAQ
RedirectPermanent /site/docs/wine-faq/index     https://wiki.winehq.org/FAQ
RedirectPermanent /screenshots                  https://appdb.winehq.org/
RedirectPermanent /site/screenshots             https://appdb.winehq.org/
RedirectPermanent /howto                        https://wiki.winehq.org/HowTo
RedirectPermanent /site/howto                   https://wiki.winehq.org/HowTo
RedirectPermanent /myths                        https://wiki.winehq.org/Debunking_Wine_Myths
RedirectPermanent /site/myths                   https://wiki.winehq.org/Debunking_Wine_Myths
RedirectPermanent /cvs                          https://wiki.winehq.org/GitWine
RedirectPermanent /site/cvs                     https://wiki.winehq.org/GitWine
RedirectPermanent /git                          https://wiki.winehq.org/GitWine
RedirectPermanent /site/git                     https://wiki.winehq.org/GitWine
RedirectPermanent /site/who                     https://wiki.winehq.org/WhosWho
RedirectPermanent /todo                         https://wiki.winehq.org/KnownIssues
RedirectPermanent /site/history                 https://wiki.winehq.org/WineHistory
RedirectPermanent /fun_projects                 https://wiki.winehq.org/Fun_Projects
RedirectPermanent /winelib                      https://wiki.winehq.org/Winelib
RedirectPermanent /license                      https://wiki.winehq.org/Licensing
RedirectMatch     ^/download/deb.*$             https://wiki.winehq.org/Download
RedirectMatch     ^/site/wineconf/?$            https://wiki.winehq.org/WineConf
RedirectMatch     ^/wineconf/?$                 https://wiki.winehq.org/WineConf

# fix documentation links
RedirectMatch ^/docs/wineusr-guide.*$           https://wiki.winehq.org/Wine_User's_Guide
RedirectMatch ^/docs/winelib-guide.*$           https://wiki.winehq.org/Winelib_User's_Guide
RedirectMatch ^/docs/winedev-guide.*$           https://wiki.winehq.org/Wine_Developer's_Guide
RedirectMatch ^/site/docs.*$                    https://www.winehq.org/documentation
RedirectMatch ^/docs.*$                         https://www.winehq.org/documentation
RedirectMatch ^/Docs.*$                         https://www.winehq.org/documentation

# redirect winehq services to appropriate web sites
RedirectMatch ^/(APPS|Apps|apps)$               https://appdb.winehq.org/
RedirectMatch ^/(APPS|Apps|apps)/query\.cgi$    https://appdb.winehq.org/
RedirectMatch ^/apps\.cgi$                      https://appdb.winehq.org/
RedirectMatch ^/(FAQ|Faq)$                      https://wiki.winehq.org/FAQ
RedirectMatch ^/(BUGS|Bugs|bugs)$               https://bugs.winehq.org/
RedirectMatch ^/(TROUBLE|Trouble|trouble)$      https://wiki.winehq.org/FAQ

# redirect source website
RedirectMatch ^/WineAPI(.*)$                    https://source.winehq.org/WineAPI$1
RedirectMatch ^/source(.*)$                     https://source.winehq.org/source$1
RedirectMatch ^/cvsweb(.*)$                     https://cvs.winehq.org/cvsweb$1
RedirectMatch ^/patch.py(.*)$                   https://cvs.winehq.org/patch.py$1

# redirect source website utils
RedirectPermanent /ident                        https://source.winehq.org/ident
RedirectPermanent /find                         https://source.winehq.org/find
RedirectPermanent /diff                         https://source.winehq.org/search

# Enable re-writing of URLs to hide the main script
RewriteEngine on
Rewritebase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.+) site.php/$1 [L]


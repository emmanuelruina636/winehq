<!--TITLE:[Live Community Chat]-->

<h1 class="title">Live Community Chat</h1>

<p>Auf <a href="https://libera.chat/">Libera.Chat</a> gibt es mehrere
IRC-Kan&auml;le f&uuml;r Wine. Den Chatraum kannst du mit einem IRC-Programm betreten, z.B.
<a href="https://hexchat.github.io/">HexChat</a>. Nutze folgende Einstellungen:</p>

<blockquote>
    <b>Server:</b> irc.libera.chat<br>
    <b>Port:</b> 6697<br>
    <b>TLS:</b> AN
</blockquote>

<p>Welchen Kanal du w&auml;hlen solltest h&auml;ngt davon ab,
welches Thema du er&ouml;rtern m&ouml;chtest:</p>

<blockquote>
    <b>#winehq:</b> Unterstützung und Hilfe von anderen Benutzern beim Ausf&uuml;hren von Wine <br>
    <b>#crossover:</b> Benutzer-Support und Hilfe beim Ausf&uuml;hren von CrossOver <br>
    <b>#winehackers:</b> Entwicklung und andere Möglichkeiten um einen Beitrag f&uuml;r Wine zu leisten<br>
    <b>#winehq-social:</b> Ungezwungene, andere Themen betreffende Chats, mit verschiedenen Community Mitgliedern<br>
</blockquote>

<p>Benutzer eines Browsers (z.B. Firefox), der IRC-URLs unterst&uuml;tzt,
k&ouml;nnen an einem Chat teilnehmen indem sie ihn anklicken:</p>
<ul>
    <li><a href="irc://irc.libera.chat/winehq">#winehq</a></li>
    <li><a href="irc://irc.libera.chat/crossover">#crossover</a></li>
    <li><a href="irc://irc.libera.chat/winehackers">#winehackers</a></li>
    <li><a href="irc://irc.libera.chat/winehq-social">#winehq-social</a></li>
</ul>

<p>Damit Diskussionen so fokussiert und hilfreich wie m&ouml;glich bleiben, versuche
deine Fragen zu recherchieren, bevor du jemanden im IRC danach fragst. Das
<a href="https://wiki.winehq.org/FAQ">Wine FAQ</a>,
<a href="//appdb.winehq.org">AppDB</a>, und die
<a href="//www.winehq.org/download">Downloadseite</a> sind gute
Optionen, um deine Frage vielleicht selbst zu beantworten.</p>

<h2>IRC-Regeln und -Strafen</h2>

<p> Abgesehen davon, dass man sich nicht beleidigend oder offensichtlich R&uuml;cksichtslos
verhalten soll, gibt es ein Paar einfache Regeln, die jeder auf IRC einzuhalten hat.
In den meisten F&auml;llen wird ein erster Regelbruch als Versehen aufgefasst und es wird eine
Verwarnung geben. Nachdem du deine Warnungen allerdings aufgebraucht hast, wirst du aus dem Kanal
geworfen.</p>

<p>Wenn du zu oft rausgeworfen wurdest und weiterhin Regeln brichst, wirst du
f&uuml;r zwei Stunden vom Kanal verbannt. Jegliche Probleme danach
f&uuml;hren zu einer endg&uuml;ltigen Verbannung, welche nur durch einen Einspruch gekippt
werden kann. Wenn du einen Rauswurf anfechten m&ouml;chtest, gehe auf <b>#winehq-social</b>
(oder auf die <a href="mailto:wine-devel@winehq.org">Wine-Devel Mailingliste</a>, wenn du
von <b>#winehq-social</b> verbannt wurdest), und erkl&auml;re warum du glaubst, dass dies passiert
ist und warum die Verbannung aufgehoben werden sollte.</p>

<table class="table">
<thead>
    <tr class="black inverse small">
        <th>Regel</th>
        <th>Erl&auml;uterung</th>
        <th>Verwarnungen</th>
        <th>Rausw&uuml;rfe</th>
    </tr>
</thead>
<tbody>
    <tr
        <td>Bitte nicht spammen.</td>
        <td></td>
        <td>1</td>
        <td>2</td>
    </tr>
    <tr>
        <td>Nutze ein Pastebin, um mehr als eine oder zwei Zeilen zu schicken.</td>
        <td></td>
        <td>0</td>
        <td>5</td>
     </tr>
    <tr>
        <td>Schreibe im passenden Kanal.</td>
        <td>Wenn du unsicher bist, frage bei <b>#winehq</b> welcher Kanal der richtige ist.</td>
        <td>2</td>
        <td>3</td>
    </tr>
    <tr>
        <td>Nur Wine und CrossOver werden in den jeweiligen Kan&auml;len
            unterst&uuml;tzt.</td>
        <td>Sidenet, WineDoors, Cedega, IEs4Linux, etc. werden <b>nicht</b>
            unterst&uuml;tzt.</td>
        <td>2</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Bevor du auf <b>#winehq</b> nach Hilfe fragst, &uuml;berpr&uuml;fe, ob du die
            neueste Version von Wine nutzt.</td>
        <td>Wenn du dir unsicher bist, führe <tt>wine --version</tt> in der Befehlszeile aus, um
            deine Version zu ermitteln</td>
        <td>3</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Bitte warte, bis du an der Reihe bist.</td>
        <td></td>
        <td>3</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Diskutiere <b>nicht</b> &uuml;ber Raubkopien.</td>
        <td></td>
        <td>1</td>
        <td>1</td>
    </tr>
</tbody>
</table>

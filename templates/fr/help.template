<!--TITLE:[Obtenir de l'aide]-->

<h1 class="title">Obtenir de l'aide</h1>

<p>Il y a plusieurs façons d'obtenir de l'aide sur Wine.</p>

<div class="black inverse bold cornerround padding-sm">
    <div class="row">
        <div class="col-xs-2 col-md-1">Alternative</div>
        <div class="col-xs-10 col-md-11">Informations complémentaires</div>
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="http://www.codeweavers.com"><i class="fas fa-money-bill-alt fa-2x"></i><br>Payant</a>
    </div>
    <div class="col-xs-10 col-md-11">
        <a href="http://www.codeweavers.com">CodeWeavers</a> propose du support
payant pour Wine.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/FAQ_fr"><i class="fas fa-info-circle fa-2x"></i><br>FAQ</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Des réponses rapides à la plupart des questions sont disponibles dans la
        <a href="https://wiki.winehq.org/FAQ_fr">FAQ</a>.
        C'est le premier endroit à visiter pour obtenir une réponse à vos questions.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/documentation"><i class="fas fa-folder-open fa-2x"></i><br>Docs</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Vous devriez lire notre <a href="{$root}/documentation">documentation</a>
        en ligne pour trouver des réponses à vos questions. Notre
        <a href="https://wiki.winehq.org/HowTo">guide pratique</a> contient également
        diverses informations utiles pour le nouvel utilisateur.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/"><i class="fas fa-book fa-2x"></i><br>Wiki</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Notre <a href="https://wiki.winehq.org/PageD'Accueil">wiki</a> peut aussi vous aider.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//forums.winehq.org/"><i class="fas fa-comments fa-2x"></i><br>Forum</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Il existe une communauté <a href="//forums.winehq.org/">web</a>/<a
href="{$root}/forums">e-mail</a> pour les utilisateurs de Wine, où ils
peuvent poser des questions et communiquer.
Vous pouvez également examiner les <a href="{$root}/forums">listes de
discussion</a> qui font également partie de la communauté.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/irc"><i class="fas fa-comment-alt fa-2x"></i><br>IRC</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Vous pouvez discuter en direct avec la communauté Wine via notre <a
href="{$root}/irc">canal IRC</a>.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//appdb.winehq.org/"><i class="fas fa-database fa-2x"></i><br>AppDB</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Si vous cherchez de l'aide pour une application particulière, notre <a
href="//appdb.winehq.org/">base de données d'applications</a> peut être
intéressante.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//bugs.winehq.org/"><i class="fas fa-bug fa-2x"></i><br>Bugzilla</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Notre <a href="//bugs.winehq.org/">système Bugzilla de suivi des bugs</a>
présente tous les problèmes sur lesquels nous avons travaillé ou travaillons actuellement.
    </div>
</div>

<p>Des problèmes avec notre site web&nbsp;? Une faute de frappe&nbsp;? Contactez
(en anglais) notre <a href="mailto:web-admin@winehq.org">équipe web</a>&nbsp;!</p>

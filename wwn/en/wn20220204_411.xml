<kc>
  <title>Wine Traffic</title>
  <author contact="mailto:wwn@winehq.org">Cressida Silver</author>
  <issue num="411" date="02/04/2022"/>
  <intro>

    <p>
      This is the 411th issue of the World Wine News publication. Its main goal is to inform you of what's going on
      around Wine. Wine is an open source implementation of the Windows API on top of X and Unix. Think of it as a
      Windows compatibility layer. Wine does not require Microsoft Windows, as it is a completely alternative
      implementation consisting of 100% Microsoft-free code.
    </p>

  </intro>

  <section title="DTLS" subject="" archive="" posts="">

    <p>
      The Datagram Transport Layer Security (DTLS) protocol provides communications privacy for datagram-based applications. No, Datagram isn't the latest online
      craze (re: Wordle)... Coined in the 1970's, Datagram is a <a href="https://en.wikipedia.org/wiki/Portmanteau">portmanteau</a> of <i>data</i> and
      <i>telegram.</i> Side note, my favorite compound word is <a href="https://en.wiktionary.org/wiki/Fu%C3%9Fg%C3%A4ngerzone">Fußgängerzone.</a> I digress.
    </p>

    <p>
      "Great, Cressida. Thanks for that... can you tell us what it all means for Wine?" Yes. Yes, I can. DTLS is a User Datagram Protocol (UDP), and
      UDP is subject to reodering, errors, duplication, and packet loss. To solve for this problem, Wine Hackers added support for DTLS packet
      retransmission. Additionally, a TLS/DTLS contextual buffer count is now cleared to alert applications that no data has been generated during a
      security context creation. Without this, some applications weren't creating an early DTLS context because the application was operating as though an
      alert existed.
    </p>

    <p>
      All this means real progress for games using Microsoft's Azure PlayFab multiplayer API (e.g., Doom Eternal, Sea of Thieves, No Man's Sky, and
      Tinkertown).
    </p>

  </section>

  <section title="WebSockets" subject="" archive="" posts="">
    <p>
      The WebSocket API allows for an interactive two-way communication session between a user's browser and a server. Microsoft introduced the <i>WebSocket</i>
      element in Windows 8 via Windows HTTP Services (WinHTTP). Wine's WinHTTP implementation began supporting WebSockets in mid 2020.
    </p>

    <p>
      The Wine 7.1 release contains updates to Wine's WinHTTP implementation, mostly around asynchronous send request handling and WinHTTP WebSocket shutdown sequence. Aside from fixing a few bugs from the initial implementation, these updates improve the performance of games that use Microsoft Azure PlayFab and depend on WinHTTP WebSocket support (e.g., Halo Infinite and Sea of Thieves).
    </p>
  </section>


  <section title="Bugs on a Table #9" subject="" archive="" posts="">

    <p>
      If there is one thing I learned in Art Appreciation class, it is that adding a number to a word or string of words elevates even the most mundane and pedestrian (see what I did there?) of artifacts to an almost etherial plane. With that in mind, I present <i>Bugs on a Table #9</i> for your consideration.
    </p>

    <center>
      <table cellspacing="0" cellpadding="3" bordercolor="#222222" border="1">
          <tr>
            <td width="100" align="center">
              <b>Bug ID</b>
            </td>
            <td width="100" align="center">
              <b>Open Date</b>
            </td>
            <td width="100" align="center">
              <b>Days Open</b>
            </td>
            <td width="100" align="center">
              <b>Weeks Open</b>
            </td>
            <td width="100" align="center">
              <b>Months Open</b>
            </td>
            <td width="100" align="center">
              <b>Years Open</b>
            </td>
          </tr>
          <tr>
            <td align="center">
              <a href ="https://bugs.winehq.org/show_bug.cgi?id=10924">10924</a>
            </td>
            <td align="center">12/28/07</td>
            <td align="center">5145</td>
            <td align="center">735</td>
            <td align="center">169</td>
            <td align="center">14</td>
          </tr>
          <tr>
            <td align="center">
              <a href ="https://bugs.winehq.org/show_bug.cgi?id=15635">15635</a>
            </td>
            <td align="center">10/16/08</td>
            <td align="center">4852</td>
            <td align="center">693</td>
            <td align="center">159</td>
            <td align="center">13</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=20415">20415</a>
            </td>
            <td align="center">10/18/09</td>
            <td align="center">4485</td>
            <td align="center">641</td>
            <td align="center">147</td>
            <td align="center">12</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=21935">21935</a>
            </td>
            <td align="center">3/5/10</td>
            <td align="center">4347</td>
            <td align="center">621</td>
            <td align="center">142</td>
            <td align="center">11</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=25053">25053</a>
            </td>
            <td align="center">11/6/10</td>
            <td align="center">4101</td>
            <td align="center">586</td>
            <td align="center">134</td>
            <td align="center">11</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=27679">27679</a>
            </td>
            <td align="center">7/3/11</td>
            <td align="center">3862</td>
            <td align="center">552</td>
            <td align="center">126</td>
            <td align="center">10</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=33756">33756</a>
            </td>
            <td align="center">6/8/13</td>
            <td align="center">3156</td>
            <td align="center">451</td>
            <td align="center">103</td>
            <td align="center">8</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=34753">34753</a>
            </td>
            <td align="center">10/17/13</td>
            <td align="center">3025</td>
            <td align="center">432</td>
            <td align="center">99</td>
            <td align="center">8</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=35063">35063</a>
            </td>
            <td align="center">12/6/13</td>
            <td align="center">2975</td>
            <td align="center">425</td>
            <td align="center">97</td>
            <td align="center">8</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=39795">39795</a>
            </td>
            <td align="center">12/14/15</td>
            <td align="center">2237</td>
            <td align="center">320</td>
            <td align="center">73</td>
            <td align="center">6</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=42660">42660</a>
            </td>
            <td align="center">3/16/17</td>
            <td align="center">1779</td>
            <td align="center">254</td>
            <td align="center">58</td>
            <td align="center">4</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=43899">43899</a>
            </td>
            <td align="center">10/19/17</td>
            <td align="center">1562</td>
            <td align="center">223</td>
            <td align="center">51</td>
            <td align="center">4</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=45016">45016</a>
            </td>
            <td align="center">4/18/18</td>
            <td align="center">1381</td>
            <td align="center">197</td>
            <td align="center">45</td>
            <td align="center">3</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=45597">45597</a>
            </td>
            <td align="center">8/9/18</td>
            <td align="center">1268</td>
            <td align="center">181</td>
            <td align="center">41</td>
            <td align="center">3</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=47975">47975</a>
            </td>
            <td align="center">10/20/19</td>
            <td align="center">831</td>
            <td align="center">119</td>
            <td align="center">27</td>
            <td align="center">2</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=48523">48523</a>
            </td>
            <td align="center">1/26/20</td>
            <td align="center">733</td>
            <td align="center">105</td>
            <td align="center">24</td>
            <td align="center">2</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=48606">48606</a>
            </td>
            <td align="center">2/13/20</td>
            <td align="center">715</td>
            <td align="center">102</td>
            <td align="center">23</td>
            <td align="center">1</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=49213">49213</a>
            </td>
            <td align="center">5/21/20</td>
            <td align="center">617</td>
            <td align="center">88</td>
            <td align="center">20</td>
            <td align="center">1</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=50370">50370</a>
            </td>
            <td align="center">12/21/20</td>
            <td align="center">403</td>
            <td align="center">58</td>
            <td align="center">13</td>
            <td align="center">1</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=50433">50433</a>
            </td>
            <td align="center">1/1/21</td>
            <td align="center">392</td>
            <td align="center">56</td>
            <td align="center">12</td>
            <td align="center">1</td>
          </tr>
          <tr>
            <td align="center">
              <a href ="https://bugs.winehq.org/show_bug.cgi?id=50539">50539</a>
            </td>
            <td align="center">1/21/21</td>
            <td align="center">372</td>
            <td align="center">53</td>
            <td align="center">12</td>
            <td align="center">1</td>
          </tr>
          <tr>
            <td align="center">
              <a href ="https://bugs.winehq.org/show_bug.cgi?id=50544">50544</a>
            </td>
            <td align="center">1/22/21</td>
            <td align="center">371</td>
            <td align="center">53</td>
            <td align="center">12</td>
            <td align="center">1</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=50849">50849</a>
            </td>
            <td align="center">3/23/21</td>
            <td align="center">311</td>
            <td align="center">44</td>
            <td align="center">10</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=50901">50901</a>
            </td>
            <td align="center">3/31/21</td>
            <td align="center">303</td>
            <td align="center">43</td>
            <td align="center">9</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51163">51163</a>
            </td>
            <td align="center">5/21/21</td>
            <td align="center">252</td>
            <td align="center">36</td>
            <td align="center">8</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51354">51354</a>
            </td>
            <td align="center">6/28/21</td>
            <td align="center">214</td>
            <td align="center">31</td>
            <td align="center">7</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51491">51491</a>
            </td>
            <td align="center">7/20/21</td>
            <td align="center">192</td>
            <td align="center">27</td>
            <td align="center">6</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51706">51706</a>
            </td>
            <td align="center">8/30/21</td>
            <td align="center">151</td>
            <td align="center">22</td>
            <td align="center">4</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51758">51758</a>
            </td>
            <td align="center">9/15/21</td>
            <td align="center">135</td>
            <td align="center">19</td>
            <td align="center">4</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51860">51860</a>
            </td>
            <td align="center">10/10/21</td>
            <td align="center">110</td>
            <td align="center">16</td>
            <td align="center">3</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51868">51868</a>
            </td>
            <td align="center">10/12/21</td>
            <td align="center">108</td>
            <td align="center">15</td>
            <td align="center">3</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=51900">51900</a>
            </td>
            <td align="center">10/18/21</td>
            <td align="center">102</td>
            <td align="center">15</td>
            <td align="center">3</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52062">52062</a>
            </td>
            <td align="center">11/19/21</td>
            <td align="center">70</td>
            <td align="center">10</td>
            <td align="center">2</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52158">52158</a>
            </td>
            <td align="center">12/3/21</td>
            <td align="center">56</td>
            <td align="center">8</td>
            <td align="center">1</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52163">52163</a>
            </td>
            <td align="center">12/4/21</td>
            <td align="center">55</td>
            <td align="center">8</td>
            <td align="center">1</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52261">52261</a>
            </td>
            <td align="center">12/23/21</td>
            <td align="center">36</td>
            <td align="center">5</td>
            <td align="center">1</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52285">52285</a>
            </td>
            <td align="center">12/26/21</td>
            <td align="center">33</td>
            <td align="center">5</td>
            <td align="center">1</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52298">52298</a>
            </td>
            <td align="center">12/29/21</td>
            <td align="center">30</td>
            <td align="center">4</td>
            <td align="center">0</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52339">52339</a>
            </td>
            <td align="center">1/4/22</td>
            <td align="center">24</td>
            <td align="center">3</td>
            <td align="center">0</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52383">52383</a>
            </td>
            <td align="center">1/11/22</td>
            <td align="center">17</td>
            <td align="center">2</td>
            <td align="center">0</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52426">52426</a>
            </td>
            <td align="center">1/20/22</td>
            <td align="center">8</td>
            <td align="center">1</td>
            <td align="center">0</td>
            <td align="center">0</td>
          </tr>
          <tr>
            <td align="center">
              <a href="https://bugs.winehq.org/show_bug.cgi?id=52446">52446</a>
            </td>
            <td align="center">1/22/22</td>
            <td align="center">6</td>
            <td align="center">1</td>
            <td align="center">0</td>
            <td align="center">0</td>
          </tr>
      </table>
    </center>
  </section>
</kc>
